
# Python
# Define a class for the shopping cart
class ShoppingCart:
  # Define a list to store items
  items = ["Iphone", "ipad", "ipod touch", "cases"]

  # Define a method to add items to the cart
  def add_item(self, item):
    self.items.append(item)
  
  # Define a method to remove items from the cart
  def remove_item(self, item):
    self.items.remove(item)

  # Define a method to get the total price of the items in the cart
  def get_total_price(self):
    total_price = 0
    for item in self.items:
      total_price += item.price
    return total_price
