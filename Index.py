# Shopping cart program coded in Python

# Create empty cart
cart = []

# Function to add item to cart
def add_item_to_cart(item_name, quantity, price):
    cart.append({"item_name": item_name, "quantity": quantity, "price": price})

# Function to remove item from cart
def remove_item_from_cart(item_name):
    for item in cart:
        if item["item_name"] == item_name:
            cart.remove(item)

# Function to calculate total cost
def calculate_total_cost():
    total = 0
    for item in cart:
        total += item["quantity"] * item["price"]
    return total

# Call functions
add_item_to_cart("Apple", 10, 0.50)
add_item_to_cart("Orange", 5, 0.75)
remove_item_from_cart("Apple")
total_cost = calculate_total_cost()

print(cart)
print(total_cost)
